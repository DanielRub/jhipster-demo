import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { KakasuSharedModule } from 'app/shared/shared.module';
import { KakasuCoreModule } from 'app/core/core.module';
import { KakasuAppRoutingModule } from './app-routing.module';
import { KakasuHomeModule } from './home/home.module';
import { KakasuEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MenuComponent } from './layouts/menu/menu.component';

@NgModule({
  imports: [
    BrowserModule,
    KakasuSharedModule,
    KakasuCoreModule,
    KakasuHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    KakasuEntityModule,
    KakasuAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent, MenuComponent],
  bootstrap: [MainComponent],
})
export class KakasuAppModule {}
