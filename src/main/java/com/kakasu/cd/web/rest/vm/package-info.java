/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kakasu.cd.web.rest.vm;
